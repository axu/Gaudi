#####################################################################################
# (c) Copyright 1998-2019 CERN for the benefit of the LHCb and ATLAS collaborations #
#                                                                                   #
# This software is distributed under the terms of the Apache version 2 licence,     #
# copied verbatim in the file "LICENSE".                                            #
#                                                                                   #
# In applying this licence, CERN does not waive the privileges and immunities       #
# granted to it by virtue of its status as an Intergovernmental Organization        #
# or submit itself to any jurisdiction.                                             #
#####################################################################################
stages:
  - build
  - test
  - deploy

image: gitlab-registry.cern.ch/lhcb-core/lbdocker/centos7-build

variables:
  NO_LBLOGIN: "1"
  TARGET_BRANCH: master
  BINARY_TAG: x86_64-centos7-gcc9-opt
  BUILDDIR: build-opt
  TESTS_REPORT: "test_report"
  LCG_hostos: "x86_64-centos7"
  LCG_release_area: "/cvmfs/sft.cern.ch/lcg/releases"
  LCG_contrib: "/cvmfs/sft.cern.ch/lcg/contrib"
  CCACHE_VERSION: "3.7.1-7651f"

build:gcc8:opt:
  stage: build
  tags:
    - cvmfs
  variables:
    BINARY_TAG: x86_64-centos7-gcc8-opt
    BUILDDIR: build-opt8
  script:
    - ci-utils/build
  artifacts:
    paths:
      - ${BUILDDIR}
    expire_in: 1 week
  cache:
    key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
    paths:
      - .ccache

build:gcc9:opt:
  stage: build
  tags:
    - cvmfs
  variables:
    BINARY_TAG: x86_64-centos7-gcc9-opt
    BUILDDIR: build-opt
  script:
    - ci-utils/build
  artifacts:
    paths:
      - ${BUILDDIR}
    expire_in: 1 week
  cache:
    key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
    paths:
      - .ccache
    
build:gcc9:dbg:
  stage: build
  tags:
    - cvmfs
  variables:
    BINARY_TAG: x86_64-centos7-gcc9-dbg
    BUILDDIR: build-dbg
  script:
    - ci-utils/build
  artifacts:
    paths:
      - ${BUILDDIR}
    expire_in: 1 week
  cache:
    key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
    paths:
      - .ccache

build:gcc9:opt:python3:
  stage: build
  tags:
    - cvmfs
  variables:
    BINARY_TAG: x86_64-centos7-gcc9-opt
    BUILDDIR: build-opt-python3
    HEPTOOLS_VERSION: 96bpython3
  script:
    - ci-utils/build
  artifacts:
    paths:
      - ${BUILDDIR}
    expire_in: 1 week
  cache:
    key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
    paths:
      - .ccache

check-formatting:
  stage: build
  dependencies: []
  image: gitlab-registry.cern.ch/gaudi/gaudi/format-checker:latest
  script:
    - ci-utils/check-formatting
  artifacts:
    paths:
      - apply-formatting.patch
    when: on_failure
    expire_in: 1 day

check-copyright:
  image: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
  script:
    - curl -o lb-check-copyright "https://gitlab.cern.ch/lhcb-core/LbDevTools/raw/master/LbDevTools/SourceTools.py?inline=false"
    - python lb-check-copyright origin/${TARGET_BRANCH}

doxygen:
  stage: test
  dependencies:
    - build:gcc9:opt
  tags:
    - cvmfs
  only:
    - master
    - tags
  variables:
    BINARY_TAG: x86_64-centos7-gcc9-opt
    BUILDDIR: build-opt
  script:
    - . ci-utils/env_setup.sh
    - find ${BUILDDIR} -type f -exec touch -d $(date +@%s) \{} \;
    - make BUILDDIR=${BUILDDIR} doc
    - rm -rf public
    - mkdir -p public/doxygen
    - mv ${BUILDDIR}/doxygen/html ${CI_COMMIT_REF_SLUG}
    - zip -r -q public/doxygen/${CI_COMMIT_REF_SLUG}.zip ${CI_COMMIT_REF_SLUG}
  artifacts:
    paths:
      - public
    expire_in: 1 day

test:gcc8:opt:
  stage: test
  dependencies:
    - build:gcc8:opt
  tags:
    - cvmfs
  variables:
    BINARY_TAG: x86_64-centos7-gcc8-opt
    BUILDDIR: build-opt8
  script:
    - ci-utils/test
  artifacts:
    paths:
      - ${TESTS_REPORT}
    when: always
    expire_in: 1 week

test:gcc9:opt:
  stage: test
  dependencies:
    - build:gcc9:opt
  tags:
    - cvmfs
  variables:
    BINARY_TAG: x86_64-centos7-gcc9-opt
    BUILDDIR: build-opt
  script:
    - ci-utils/test
  artifacts:
    paths:
      - ${TESTS_REPORT}
    when: always
    expire_in: 1 week

test:gcc9:dbg:
  stage: test
  dependencies:
    - build:gcc9:dbg
  tags:
    - cvmfs
  variables:
    BINARY_TAG: x86_64-centos7-gcc9-dbg
    BUILDDIR: build-dbg
  script:
    - ci-utils/test
  artifacts:
    paths:
      - ${TESTS_REPORT}
    when: always
    expire_in: 1 week

test:gcc9:opt:python3:
  stage: test
  dependencies:
    - build:gcc9:opt:python3
  tags:
    - cvmfs
  variables:
    BINARY_TAG: x86_64-centos7-gcc9-opt
    BUILDDIR: build-opt-python3
  script:
    - ci-utils/test
  artifacts:
    paths:
      - ${TESTS_REPORT}
    when: always
    expire_in: 1 week

test_public_headers_build:
  stage: test
  dependencies:
    - build:gcc9:opt
  tags:
    - cvmfs
  variables:
    BINARY_TAG: x86_64-centos7-gcc9-opt
    BUILDDIR: build-opt
  script:
    - ci-utils/test_public_headers_build
  cache:
    key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
    paths:
      - .ccache

test-python3:
  image: python:3-slim
  stage: test
  script:
    - pip install nose coverage
    - PYTHONPATH=GaudiConfiguration/python nosetests -v --with-coverage --cover-package=GaudiConfig2 --cover-min-percentage=100 GaudiConfiguration/tests/nose

# see https://gitlab.cern.ch/gitlabci-examples/deploy_eos for the details
# of the configuration
deploy-doxygen:
  stage: deploy
  dependencies:
    - doxygen
  only:
    - master
    - tags
  image: gitlab-registry.cern.ch/ci-tools/ci-web-deployer:latest
  script:
    - test -z "$EOS_ACCOUNT_USERNAME" -o -z "$EOS_ACCOUNT_PASSWORD" -o -z "$EOS_PATH" && exit 0 || true
    # Script that performs the deploy to EOS. Makes use of the variables defined in the project
    # It will copy the generated content to the folder in EOS
    - deploy-eos
  # do not run any globally defined before_script or after_script for this step
  before_script: []
  after_script: []
